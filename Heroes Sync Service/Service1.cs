﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Heroes_Sync_Service
{
    public partial class Service1 : ServiceBase
    {

        string logsPath = AppDomain.CurrentDomain.BaseDirectory + "Heroes Sync Serevice Logs.txt";
        static HttpClient httpClient = new HttpClient();

        public Service1()
        {
            InitializeComponent();
        }

        public void onDebug()
        {
            OnStart(null);
        }
        protected override void OnStart(string[] args)
        {

            custTextWriter("Service Started :" + DateTime.Now.ToString("yyyyMMdd hh:ss:mm"));

            startSyncprocess();

        }

        private void startSyncprocess()
        {
            custTextWriter("Sync Process Started : " + DateTime.Now.ToString("yyyyMMdd hh:ss:mm"));

            initiateLooper();
        }

        private void initiateLooper()
        {
            while (true)
            {
                makeSyncHttpGetReqAsync();
                Thread.Sleep(600000);
            }
        }

        private async Task makeSyncHttpGetReqAsync()
        {
            var BaseUrl = "http://197.248.29.18:82/";

#if DEBUG
            BaseUrl = "http://192.168.43.63:85/";
#endif
            HttpResponseMessage response = await httpClient.GetAsync(BaseUrl + "api/AppDataEndpoint");

            custTextWriter("Sync \n\tRespone code : " + response.StatusCode
                + "\n\t Response Message : " +  response.ReasonPhrase
                + "\n\t Date : " + DateTime.Now.ToString("yyyy-MM-dd hh:ss:mm")
                +"\n\t Data : " + await response.Content.ReadAsStringAsync());

        }

        private void custTextWriter(string text)
        {
            TextWriter tw = new StreamWriter(logsPath, append: true);
            tw.WriteLine(text);
            tw.Close();
        }

        protected override void OnStop()
        {
            custTextWriter("Service Stopped :" + DateTime.Now.ToString("yyyyMMdd hh:ss:mm"));
        }
    }
}
